# DNS Deprecate

Plugin for eventually phasing out the use of the domain "cubekrowd.net" to login to the server with Minecraft.

## Deprecation schedule

The following is the 6-month schedule for smoothly phasing out the use of the bare domain to connect with Minecraft.
The reason for the long delay is that some people take several months break, and we want to prevent them from thinking CubeKrowd is down when they come back. If someone tries to use the wrong domain when trying to connect after 1 September there is still a chance they can see the warning message, however, that depends on if their DNS lookup service (normally your ISP if you are not using Google DNS or an alternative DNS) supports SRV.

### 6-month schedule

**1 March** - MOTD message and LOGIN message

**1 June** - Kicked with message when trying to login

**1 September** - Activate Cloudflare protection on main domain. This will completely disable all Minecraft connections which does not use SRV.
