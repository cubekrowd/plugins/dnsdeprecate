/*

    dnsdeprecate
    Copyright (C) 2018  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.dnsdeprecate;

import java.text.*;
import java.util.*;
import java.io.*;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.connection.*;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.*;
import net.md_5.bungee.event.*;
import lombok.*;

public class DNSDeprecate extends Plugin implements Listener {

    private TextComponent msg;
    private TextComponent ping;

    @Override
    public void onEnable() {

        String l = "\n";
        ChatColor r = ChatColor.DARK_RED;
        ChatColor w = ChatColor.GOLD;
        ChatColor g = ChatColor.GRAY;
        ChatColor b = ChatColor.DARK_AQUA;
        String f = "████████████████████████████████" + l;
        msg = new TextComponent(new ComponentBuilder(l).bold(true)
                                .append(f).color(r)
                                .append(f).color(r)

                                .append("██ ").color(r)
                                .append("Warning: Use of the old \"").color(w)
                                .append("cubekrowd.net").color(g)
                                .append("\"").color(w)
                                .append("       ██" + l).color(r)

                                .append("██ ").color(r)
                                .append("address is deprecated and will stop working!").color(w)
                                .append(" ██" + l).color(r)

                                .append("██ ").color(r)
                                .append("Please use \"").color(w)
                                .append("play.cubekrowd.net").color(b)
                                .append("\" instead.").color(w)
                                .append("      ██" + l).color(r)

                                .append(f).color(r)
                                .append(f).color(r)
                                .create());

        ping = new TextComponent(new ComponentBuilder("").bold(true)

                                 .append("█ ").color(r)
                                 .append("Warning: ").color(w)
                                 .append("cubekrowd.net").color(g)
                                 .append(" is deprecated!").color(w)
                                 .append("  █" + l).color(r)

                                 .append("█ ").color(r)
                                 .append("Please use ").color(w)
                                 .append("play.cubekrowd.net").color(b)
                                 .append(" instead.").color(w)
                                 .append(" █" + l).color(r)

                                 .create());


        getProxy().getPluginManager().registerListener(this, this);

        getLogger().info("2018-03-01 = " + checkDate("2018-03-01"));
        getLogger().info("2018-05-01 = " + checkDate("2018-05-01"));
        getLogger().info("2018-06-01 = " + checkDate("2018-06-01"));
    }

    @Override
    public void onDisable() {
    }

    @EventHandler
    public void onServerConnect(ServerConnectEvent e) {
        if(checkDate("2018-03-01")) {
            if(checkHostname(e.getPlayer().getPendingConnection())) {
                e.getPlayer().sendMessage(msg);
            }
        }
    }

    @EventHandler(priority=EventPriority.HIGH)
    public void onPreLogin(PreLoginEvent e) {
        if(checkDate("2018-06-01")) {
            if(checkHostname(e.getConnection())) {
                e.setCancelled(true);
                e.setCancelReason(msg);
            }
        }
    }


    @EventHandler(priority=Byte.MAX_VALUE)
    public void onProxyPing(ProxyPingEvent e) {
        if(checkDate("2018-03-01")) {
            if(checkHostname(e.getConnection())) {
                e.getResponse().setDescriptionComponent(ping);
            }
        }
    }

    public boolean checkHostname(PendingConnection conn) {
        try {
            String hostname = conn.getVirtualHost().getHostName().toLowerCase();
            return hostname.indexOf("play") == -1 && hostname.indexOf("game") == -1;
        } catch (Exception e) {
            return true;
        }
    }


    public boolean checkDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(date).before(new Date());
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

}
